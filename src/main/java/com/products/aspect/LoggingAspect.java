package com.products.aspect;

import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Before;

public class LoggingAspect {
//	An aspect can contain no of advises
	
//To convert standard class into aspect use annotation

//@Before(value = "execution()") //before this method executes,we'll  run loggingAdvice method
/*  "execution()" --->pointCut
* means for all-->WildCard
.. means 0 arguments or any no of arguments no matter what!
if left or empty it means 0 arguments	
one advice- to run on different points or methods 


the point where we want to cut in this method is called pointCut @Pointcut
we can also use within instead execution for more readability

JoinPoint is the place where we can apply advice to any not just methods (ASPECTJ )(we can also apply to member variables before and after edition) 
*/
Log logger;
@Before(value="execution(* com.products.DAO.ProductDAO.*(..))")
public void beforeMethodCall(JoinPoint joinPoint) {
	logger=LogFactory.getLog(joinPoint.getTarget().getClass());

	logger.info("Calling method- " + joinPoint.getSignature().getName());
	logger.info("Parameters:" + Arrays.toString(joinPoint.getArgs()));

}
@AfterReturning(value="execution(* com.products.DAO.ProductDAO.*(..))", returning="result")
public void afterReturingFromMethod(JoinPoint joinPoint, Object result) {
	logger = LogFactory.getLog(joinPoint.getTarget().getClass());
	logger.info("Returning method- " + joinPoint.getSignature().getName());
	logger.info("Return value:" + result);
}
@AfterThrowing(value="execution(* com.products.DAO.ProductDAO.*(..))", throwing="ex")
public void afterExceptionThrowing(JoinPoint joinPoint, Throwable ex) {
	logger = LogFactory.getLog(joinPoint.getTarget().getClass());
	logger.info("Returning method- " + joinPoint.getSignature().getName());
	logger.error("Exception Occured:" + ex.toString());
}


}
