package com.products.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.products.model.Product;

public class ProductDAO implements IProductDAO {
	
	JdbcTemplate jdbcTemplate;



	public ProductDAO(JdbcTemplate jdbcTemplate2) {
		this.jdbcTemplate=jdbcTemplate2;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	
	public boolean insertProduct(Product prod) {
		boolean isInserted=false;
		
		int result= jdbcTemplate.update("INSERT INTO prod VALUES(?,?,?,?,?)", prod.getProductCode(),
				prod.getProductDescription(),prod.getCategoryCode(),
				prod.getPrice(),prod.getQuantityOnHand());
		
		if (result==1) {
				isInserted=true;
		}
		return isInserted;
	}

	
	public boolean deleteProduct(String productCode) {

//		Using Named Parameters
//This namedParemeters wraps up the JDBC template so we must pass that jdbc temp obj
	
		boolean isDeleted=false;
		NamedParameterJdbcTemplate ntemplate=new NamedParameterJdbcTemplate(jdbcTemplate);
		
		Map<String, String> prodMap =new HashMap<String, String>();
		prodMap.put("prodCode", productCode);
		
		int result=ntemplate.update("DELETE FROM prod WHERE product_code=:prodCode",prodMap);
		
		if(result==1)
			isDeleted=true;
		return isDeleted;
		
//		Using JDBC template
		
//		boolean isDeleted=false;
//		
//		int result=jdbcTemplate.update("DELETE FROM prod WHERE prodduct_code=?");
//		
//		if(result==1) {
//			isDeleted=true;
//		}
//		return isDeleted;
	}

	
	public List<Product> getAllProducts() {
		List<Product> productList = new ArrayList<Product>();


		productList=jdbcTemplate.query("SELECT product_code,prod_desc,category_code,price,qty_on_hand FROM "
				+ "prod",new RowMapper<Product>() {
		
		
		public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
			Product prod=new Product();
			prod.setProductCode(rs.getString(1));
			prod.setProductDescription(rs.getString(2));
			prod.setCategoryCode(rs.getString(3));
			prod.setPrice(rs.getInt(4));
			prod.setQuantityOnHand(rs.getInt(5));
			return prod;
			}
	});
		
		return productList;
	}

	
	public Product getProduct(String productCode) {
		Product product = null;
		product = jdbcTemplate.queryForObject("SELECT product_code, prod_desc, category_code, price, qty_on_hand FROM prod"
				+ " WHERE product_code = ?",
				new RowMapper<Product>() {
					
					public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
						
						Product prod = new Product();
						prod.setProductCode(rs.getString(1));
						prod.setProductDescription(rs.getString(2));
						prod.setCategoryCode(rs.getString(3));
						prod.setPrice(rs.getInt(4));
						prod.setQuantityOnHand(rs.getInt(5));
						return prod;
					}
				}, 
				productCode);
		return product;
		
	}

	
	public boolean updateProduct(String productCode, String productDescription, String category, int price,
			int quantityOnHand) {
//		Using Named Parameters 
		
//		boolean isUpdate=false;
//		NamedParameterJdbcTemplate ntemp=new NamedParameterJdbcTemplate(jdbcTemplate);
//		
//		
//		Map<String, String> productMap= new HashMap<String, String>();
//		
//		productMap.put("pcode", productCode);
//		productMap.put("pdesc", productDescription);
//		productMap.put("catCode", category);
//		
//		
//		Map<String, Integer> proMap= new HashMap<String, Integer>();
//	
//		proMap.put("pr", price);
//		proMap.put("qty", quantityOnHand);
//		
//		
//		int result=ntemp.update("UPDATE productdatabase SET prod_desc=:pdesc,category_code=:catCode WHERE product_code=:pcode", productMap);;
//	
//		int result1=ntemp.update("UPDATE productdatabase SET price=:pr,qty_on_hand=:qty  WHERE product_code=:pcode", proMap);
//		if(result==1 && result1==1)
//			isUpdate=true;
//		
//		return isUpdate;
		
		boolean isUpdate=false;
		 int result=jdbcTemplate.update("UPDATE prod SET prod_desc= ?,category_code= ?, price= ?, qty_on_hand= ?"
		 		+ " WHERE product_code=?");
		 
		if(result==1) {
			isUpdate=true;
		}
		return isUpdate;
	}

	public boolean deleteProducts(String category) {
		boolean isDeleted = false;
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(jdbcTemplate);
		
		Map<String, String> prodMap = new HashMap<String, String>();
		prodMap.put("catCode", category);
		
		int result = template.update("DELETE FROM prod WHERE category_code=:catCode", prodMap);
		
		if(result > 1)
			isDeleted = true;
		
		return isDeleted;
	
	}
}
