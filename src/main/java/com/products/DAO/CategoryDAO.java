package com.products.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.products.model.Category;

public class CategoryDAO implements ICategoryDAO{
	
	JdbcTemplate jdbcTemplate;
	

	public CategoryDAO(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public boolean addCategory(Category category) {
		boolean isAdded=false;
		
		int res=jdbcTemplate.update("INSERT INTO category VALUES(?,?)",category.getCategoryCode(),category.getCategoryDescription());
		
		if(res==1) {
			isAdded=true;
		}
		
		return isAdded;
	}


	public Category getCategory(String categoryCode) {
		Category categ = null;
		categ = jdbcTemplate.queryForObject("SELECT category_code, category_desc FROM category"
				+ " WHERE category_code = ?",
				new RowMapper<Category>() {
					
					public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
						
						Category cat = new Category();
						cat.setCategoryCode(rs.getString(1));
						cat.setCategoryDescription(rs.getString(2));
						
						return cat;
					}
				}, 
				categoryCode);
		return categ;
	}

	
	public boolean removeCategory(String categoryCode) {
		boolean isDeleted=false;
		NamedParameterJdbcTemplate ntemp=new NamedParameterJdbcTemplate(jdbcTemplate);
		
		Map<String, String> catMap =new HashMap<String, String>();
		catMap.put("catCode", categoryCode);
		
		int result=ntemp.update("DELETE FROM category WHERE category_code=:catCode",catMap);
		
		if(result==1)
			isDeleted=true;
		return isDeleted;
		
	}

	public boolean addCategory(java.util.Locale.Category category) {
		// TODO Auto-generated method stub
		return false;
	}

}
