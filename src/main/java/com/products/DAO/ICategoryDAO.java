package com.products.DAO;

import java.util.Locale.Category;

public interface ICategoryDAO {
	public boolean addCategory(Category category);
	public com.products.model.Category getCategory(String categoryCode);
	public boolean removeCategory(String categoryCode);
}
