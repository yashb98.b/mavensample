package com.products.DAO;

import java.util.List;

import com.products.model.Product;

public interface IProductDAO {
public boolean insertProduct(Product prod);
	
	public boolean deleteProduct(String productCode);
	
	public List<Product> getAllProducts();
	
	public Product getProduct(String productCode);
	
	public boolean updateProduct(String productCode,String productDescription,String category,int price,
			int quantityOnHand );
	
	
	public boolean deleteProducts(String category);

}
