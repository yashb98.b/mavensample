package com.products.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.products.config.AppConfig;
import com.products.service.IProductService;

public class TxMain {
public static void main(String[] args) {
	ApplicationContext ctx= new AnnotationConfigApplicationContext(AppConfig.class);
	
	IProductService psdao=ctx.getBean("productService",IProductService.class);

	System.out.println(psdao.getCategory("CT03"));;
//	System.out.println(psdao.removeCategory("CT01"));
}
}
