package com.products.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.products.DAO.IProductDAO;
import com.products.config.AppConfig;
import com.products.service.IProductService;

public class JDBCMain {
public static void main(String[] args) {
	ApplicationContext ctx=new  AnnotationConfigApplicationContext(AppConfig.class);
//	to get rid of sunny.proxy error always use Interfaces for proxie's but not class 
//	Does your ProductDAO class implement some interface? If yes, then you should use interface and not class in your code
//	Because if your bean implements some interface then Spring by default will create proxy based on this interface
		IProductDAO dao=(IProductDAO) ctx.getBean("productDAO");
//		System.out.println(dao.updateProduct("P23","Battery","CT02",2500,20));
		
		IProductService psDao=ctx.getBean("productService",IProductService.class);
		
//	System.out.println(psDao.getCategory("CT03"));
//System.out.println(	psDao.insertProduct(new Product("P23","Battery","CT03",2500,20)));

//		IProductDAO dao= (IProductDAO)ctx.getBean("productDAO");
		
		
//		ProductDAO dao= ctx.getBean("productDAO",ProductDAO.class);
	
//		boolean result =dao.insertProduct(new Product("P03","PROCESSOR", "CT01", 3000, 20));
//				System.out.println(result);
//		
		boolean result1=dao.deleteProduct("P23");
		
		System.out.println(result1);
			
		System.out.println(dao.getAllProducts());
		
		
		
		
//		System.out.println(dao.getProduct("P25"));

//		System.out.println(dao.deleteProduct("P25"));
		
}
}
