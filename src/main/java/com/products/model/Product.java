package com.products.model;
import org.springframework.jdbc.core.JdbcTemplate;
public class Product {
	public String productCode;
	public String productDescription;
	public String categoryCode;
	public int price;
	public int quantityOnHand;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(String productCode, String productDescription, String categoryCode, int price, int quantityOnHand) {
		super();
		this.productCode = productCode;
		this.productDescription = productDescription;
		this.categoryCode = categoryCode;
		this.price = price;
		this.quantityOnHand = quantityOnHand;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQuantityOnHand() {
		return quantityOnHand;
	}
	public void setQuantityOnHand(int quantityOnHand) {
		this.quantityOnHand = quantityOnHand;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoryCode == null) ? 0 : categoryCode.hashCode());
		result = prime * result + price;
		result = prime * result + ((productCode == null) ? 0 : productCode.hashCode());
		result = prime * result + ((productDescription == null) ? 0 : productDescription.hashCode());
		result = prime * result + quantityOnHand;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (categoryCode == null) {
			if (other.categoryCode != null)
				return false;
		} else if (!categoryCode.equals(other.categoryCode))
			return false;
		if (price != other.price)
			return false;
		if (productCode == null) {
			if (other.productCode != null)
				return false;
		} else if (!productCode.equals(other.productCode))
			return false;
		if (productDescription == null) {
			if (other.productDescription != null)
				return false;
		} else if (!productDescription.equals(other.productDescription))
			return false;
		if (quantityOnHand != other.quantityOnHand)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Product [productCode=" + productCode + ", productDescription=" + productDescription + ", categoryCode="
				+ categoryCode + ", price=" + price + ", quantityOnHand=" + quantityOnHand + "]";
	}
	
}
