package com.products.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.products.DAO.ICategoryDAO;
import com.products.DAO.IProductDAO;
import com.products.model.Category;
import com.products.model.Product;
public class ProductService  implements IProductService{
	
	IProductDAO productDAO;
	ICategoryDAO categoryDAO;
	
	
	public ProductService(IProductDAO productDAO, ICategoryDAO categoryDAO) {
		super();
		this.productDAO = productDAO;
		this.categoryDAO = categoryDAO;
	}

	public boolean insertProduct(Product prod) {
		
			
		boolean insertStatus=productDAO.insertProduct(prod);
	
	
	return insertStatus;
}
	
	public boolean deleteProduct(String productCode) {
		boolean deleteStatus=productDAO.deleteProduct(productCode);
		return deleteStatus;
	}
	
	public List<Product> getAllProducts() {
		List<Product> pList=productDAO.getAllProducts();
		return pList;
	}
	
	public Product getProduct(String productCode) {
		Product prdStatus=productDAO.getProduct(productCode);
		return prdStatus;
	}
	
	public boolean updateProduct(String productCode,String productDescription,String category,int price,
			int quantityOnHand ) {
		boolean updateStatus=productDAO.updateProduct(productCode, productDescription, category, price, quantityOnHand);
		return updateStatus;
	}
	
	public Category getCategory(String categoryCode) {
		Category catStatus=categoryDAO.getCategory(categoryCode);
		return catStatus;
		
	}
	public boolean removeCategory(String categoryCode) {

		boolean productRemoveStatus=productDAO.deleteProducts(categoryCode);
		
		boolean categoryRemoveStatus=categoryDAO.removeCategory(categoryCode);
		
		if(productRemoveStatus && categoryRemoveStatus) {
			return true;
		}
		else {
			return false;
		}
		
//		boolean productRemoveStatus = productDAO.removeProducts(category);
//		boolean categoryRemoveStatus = categoryDAO.removeCategory(category);
//		if(productRemoveStatus && categoryRemoveStatus)
//		return true;
//		else return false;
		
	
	}

	public boolean addCategory(Category category) {
		// TODO Auto-generated method stub
		return false;
	}


}

