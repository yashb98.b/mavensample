package com.products.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.products.DAO.CategoryDAO;
import com.products.DAO.ICategoryDAO;
import com.products.DAO.IProductDAO;
import com.products.DAO.ProductDAO;
import com.products.aspect.LoggingAspect;
import com.products.service.IProductService;
import com.products.service.ProductService;

public class AppConfig {
	@Autowired (required = true)
	Environment env;
	
	
	@Bean(name="loggingAspect")
	public LoggingAspect getLoggingAspect() {
		
		LoggingAspect logAspect=new LoggingAspect();
		return logAspect;
	}
	
	
	@Bean(name="dataSource")
	public DriverManagerDataSource getDataSource() {
		
		DriverManagerDataSource dataSource =new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		dataSource.setUrl(env.getProperty("jdbc.url"));
		dataSource.setUsername(env.getProperty("jdbc.user"));
		dataSource.setPassword(env.getProperty("jdbc.password"));
		return dataSource;
		
	}
	@Bean (name="jdbcTemplate")
	public JdbcTemplate getJdbcTemplate() {
		
		JdbcTemplate jdbcTemp =new JdbcTemplate();
		jdbcTemp.setDataSource(getDataSource());
		return jdbcTemp;
		
	}
	@Bean(name="productDAO")
	public IProductDAO getProductDAO() {
		
//		ProductDAO prodDao=new ProductDAO(getJdbcTemplate());
//		JdbcTemplate jdbcTemplate =new JdbcTemplate();
//		prodDao.setJdbcTemplate(getJdbcTemplate());
//		return prodDao;
//		return prodDao;
		
		IProductDAO prod=new ProductDAO(getJdbcTemplate());
	
	
//		productDAO.setJdbcTemplate(getJdbcTemplate());
		return prod;
		
	}
	
	@Bean(name="categoryDAO")
	public ICategoryDAO getCategoryDAO() {
		
		ICategoryDAO catdao=new CategoryDAO(getJdbcTemplate());
		return catdao;
	}
	@Bean(name="productService")
	public IProductService getProductService() {
		
		IProductService pService= new ProductService(getProductDAO(), getCategoryDAO());

		return pService;
		
		
	}
	
	

}
